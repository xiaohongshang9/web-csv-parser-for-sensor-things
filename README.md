# Usage #

This web application is used for in-browser csv sensor records parsing. Users can paste csv data records. The application will parse CSV to SensorThings Observations and Post observations to SensorThings service.

### How to use it? ###

* download the source scripts and put them under one folder
* open the html in browser 
* Input field as required and Click the Button to Upload

### CSV Data Requirements ###
* Open CSV data with a Text Editor(not Excel,will have comma) 
* Copy the CSV data and Paste to the Required field in the application
* CSV data should contain the sensor observation field and time field
